
<!-- README.md is generated from README.Rmd. Please edit that file -->

# FAI

<!-- badges: start -->

[![coverage
report](https://gitlab.com/rpkgs/fai/badges/main/coverage.svg)](https://gitlab.com/rpkgs/fai/-/commits/main)
[![pipeline
status](https://gitlab.com/rpkgs/fai/badges/main/pipeline.svg)](https://gitlab.com/rpkgs/fai/-/commits/main)
<!-- badges: end -->

The goal of FAI is to have an general interface for API call against
financial data provider. This package is inspired by
[DBI](https://dbi.r-dbi.org/).

## Installation

You can install the development version of `FAI` like so:

``` r
remotes::install_gitlab("rpkgs/fai")
```

## Example

Use one of the supported finance API packages to communicate to your
data provider (At the moment there is just one package ;)).

``` r
library(FAI)

con <- connect(iexFinance::Iex(), token = Sys.getenv("IEX_TOKEN_PK"))
chart <- download_chart(con, symbol = "AAPL")

plot(chart$date, chart$close, type = "l", ylab = "Price per Share", xlab = "")
```

<img src="man/figures/README-example1-1.png" width="100%" />

All functions in the format of `download_*`.

``` r
download_balance(con, symbol = "AAPL", period = "annual", last = 1)
#>   accounts_payable capital_surplus common_stock currency current_assets
#> 1       6.4115e+10              NA  16215963000      USD    1.35405e+11
#>   current_cash current_long_term_debt filing_type fiscal_date fiscal_quarter
#> 1   4.8304e+10              2.111e+10        10-K  2022-09-24              0
#>   fiscal_year goodwill intangible_assets inventory long_term_debt
#> 1        2022        0                 0 4.946e+09     9.8959e+10
#>   long_term_investments minority_interest net_tangible_assets other_assets
#> 1            2.1735e+11                 0          5.0672e+10   5.4428e+10
#>   other_current_assets other_current_liabilities other_liabilities
#> 1           2.1223e+10                6.8757e+10        4.9142e+10
#>   property_plant_equipment receivables report_date retained_earnings
#> 1               4.2117e+10  6.0932e+10  2022-10-28        -3.068e+09
#>   shareholder_equity short_term_investments symbol total_assets
#> 1         5.0672e+10             2.1223e+10   AAPL  3.52755e+11
#>   total_current_liabilities total_liabilities treasury_stock period
#> 1               1.53982e+11       3.02083e+11              0 annual
```

## Instructions for Package Developer

Like DBI the FAI package provide a lot of generic methods. These can be
implemented in other packages by importing this package to create a new
backend.

    Imports:
      FAI,
      methods

Here is an example from
[iexFinance](https://rpkgs.gitlab.io/iexfinance/) how to implement some
mandatory classes and methods.

First you have to create a Driver class that inherit “FAIDriver”.

``` r
#' Driver for IEX API
#'
#' @keywords internal
#' @export
#' @import FAI
setClass("IexDriver", contains = "FAIDriver")

#' Instantiate IexConnection class
#' @import methods
#' @export
Iex <- function() {
  new("IexDriver")
}
```

Then create a connection class with a corresponding connect method. The
connect method set the base URL and the token which is used by
[iexcloud.io](https://iexcloud.io/) in this example.

``` r
#' IEX connection class.
#'
#' @export
#' @keywords internal
setClass("IexConnection",
  contains = "FAIConnection",
  slots = list(
    token = "character",
    uri = "character"
  )
)

#' @inherit
#' FAI::connect title description params details references return seealso
#' @param token Iex api token.
#' @param sandbox Use sandbox mode to save data credits during experiments.
#' @export
setMethod("connect", "IexDriver", function(drv, token, sandbox = FALSE, ...) {
  uri <- sprintf("https://%s.iexapis.com/v1", ifelse(sandbox, "sandbox", "cloud"))
  new("IexConnection", token = token, uri = uri)
})
```

For a download\_\* method the function should have the connection object
as the first argument followed by symbol and period. You can add other
optional arguments such as `last`.

``` r
#' @inherit
#' FAI::download_balance title description params details references return seealso
#' @param last Download last n reports.
#' @export
setMethod("download_income", "IexConnection", function(drv, symbol, period = c("annual", "quarterly"), last = 1, ...) {
  # some code...
  url <- httr::modify_url(
    sprintf("%s/time-series/INCOME/%s/%s", drv@uri, symbol, period),
    query = list(
      token = drv@token,
      last = last
    )
  )
  response <- httr::GET(url)
  # some code...
  return(result)
})
```

Overview of all functions:

- `connect` Create a Connection Object for Financial APIs.
- `download_balance` Download Financial Company Reports.
- `download_cashflow` Download Financial Company Reports.
- `download_income` Download Financial Company Reports.
- `download_chart` Download Chart Data.
- `download_company` Download Company Information.
- `download_symbols` Get available symbols from the API.
- `get_cache_prefix` Get Table Name Prefix for Caching (for internal
  use).

Functions that not need to be implemented by a package:

- `download_balance_x` Download Reports by using Cache. Save API calls
  by caching downloaded data and reuse it. The functionality is simple
  and could not fit your needs. So have a look on the results.
- `download_cashflow_x` Download Reports by using Cache. Save API calls
  by caching downloaded data and reuse it. The functionality is simple
  and could not fit your needs. So have a look on the results.
- `download_income_x` Download Reports by using Cache. Save API calls by
  caching downloaded data and reuse it. The functionality is simple and
  could not fit your needs. So have a look on the results.
- `download_company_x` Download Company Information by using Cache.

Test the result object with this functions:

- Tests for `download_balance`, `download_income` and
  `download_cashflow`.
  - `expect_report_names` Expect that all column names are camel_case.
  - `expect_report_types` Expect all values are numeric, except columns
    like `report_date` or `symbol`.
