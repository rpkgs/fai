test_that("registration works", {
  # devtools::document() # nolint
  # devtools::load_all() # nolint
  # logger::log_threshold(logger::DEBUG) # nolint

  # db <- DBI::dbConnect(RSQLite::SQLite(), dbname = ":memory:")
  # obj <- connector(connector = "iex", token = Sys.getenv("IEX_TOKEN_PK"))
  # obj <- set_cache(obj, db)

  # symbols <- download(obj, type = "symbol")
  # income <- download(obj, type = "income", symbol = c("AAPL", "ACIW"), period = "quarterly")
  # balance <- download(obj, type = "balance", symbol = c("AAPL", "ACIW"), period = "quarterly")
  # cashflow <- download(obj, type = "cashflow", symbol = c("AAPL", "ACIW"), period = "quarterly")
  # charts <- download(obj, type = "chart", symbol = c("AAPL", "ACIW"))
  # stats <- download(obj, type = "stats", symbol = c("AAPL", "ACIW"))
})

test_that("error handling works", {
  # devtools::document() # nolint
  # devtools::load_all() # nolint
  # logger::log_threshold(logger::DEBUG) # nolint

  debugonce(download)
  obj_invalid <- connector(connector = "iex", token = "no_valid_key")
  expect_error(download(obj_invalid, type = "income", symbol = c("ALYA")), regexp = "Forbidden")

  obj_invalid <- connector(connector = "iex", token = "no_valid_key", skip_on_error = TRUE)
  expect_error(download(obj_invalid, type = "income", symbol = c("ALYA"), period = "quarterly"), regexp = "Forbidden")

  obj <- connector(connector = "iex", token = Sys.getenv("IEX_TOKEN_PK"))
  expect_error(download(obj, type = "income", symbol = c("ALYA"), period = "quarterly"), regexp = "No data found for")

  obj <- connector(connector = "iex", token = Sys.getenv("IEX_TOKEN_PK"), skip_on_error = TRUE)
  expect_null(download(obj, type = "income", symbol = c("ALYA"), period = "quarterly"))

  income <- download(obj, type = "income", symbol = c("AAPL"), period = "quarterly")
  expect_s3_class(income, "data.frame")
  expect_snapshot(sapply(income, class))

})


