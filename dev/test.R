

library(RSQLite)
library(memoise)
library(rlang)
library(httr)

# Set up the cache database
con <- dbConnect(RSQLite::SQLite(), dbname = "api_cache.sqlite")

# Set up memoisation
api_request <- memoise(api_request)

get_income_statements <- function(api_key, symbol, period = "quarter", last = 1) {
  # Build the URL for the API request
  url <- paste0("https://cloud.iexapis.com/stable/stock/",
                symbol,
                "/income?period=", period,
                "&last=", last,
                "&token=", api_key)

  # Make the API request
  response <- api_request(url)

  # Return the data as a data frame
  as.data.frame(response)
}



source("dev/api_request.R")
source("dev/cache.R")
my_api_key <- "Tpk_ce28e11cc96e4ced96210e4ae565f781"
symbol <- "AAPL"
period <- "quarter"
last <- 1

income_statements <- get_income_statements(api_key = my_api_key,
                                           symbol = symbol,
                                           period = period,
                                           last = last)



api_key <- "Tpk_ce28e11cc96e4ced96210e4ae565f781" #Sys.getenv("IEX_TOKEN_PK")
Sys.setenv(IEXCLOUD_API_KEY = api_key)
symbol <- "AAPL"
debugonce(get_income_statements)
income_statement_data <- get_income_statements(symbol)
