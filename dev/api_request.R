api_request <- function(url, api_key) {
  # Check if the response is already in the cache
  cache_key <- url
  if (cache_exists(cache_key)) {
    return(dbGetQuery(con, paste0("SELECT * FROM api_cache WHERE key = '", cache_key, "'"))$value)
  }

  # Make the API request
  response <- httr::GET(url)

  # Check for errors
  if (response$status_code != 200) {
    message(response$content)
    abort("API request failed with status code ", response$status_code)
  }

  # Extract the data from the response
  data <- httr::content(response)

  # Save the response to the cache
  dbGetQuery(con, paste0("INSERT INTO api_cache (key, value) VALUES ('", cache_key, "', '", RSQLite::dbEscapeStrings(con, serialize(data, NULL, FALSE)), "')"))

  # Return the data
  data
}
