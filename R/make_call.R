#' Make call expression
#'
#' @param fun A character string of a function name.
#' @param ns A character string of a package name.
#' @param args A named list of argument values.
#' @details The arguments are spliced into the `ns::fun()` call. If they are
#' missing, null, or a single logical, then are not spliced.
#' @return A call.
#' @keywords internal
make_call <- function(fun, ns = NULL, args, ...) {
  # remove any null or placeholders (`missing_args`) that remain
  discard <- vapply(args, \(x) rlang::is_missing(x) | is.null(x), logical(1))
  args <- args[!discard]
  if (!is.null(ns) && !is.na(ns)) {
    out <- rlang::call2(fun, !!!args, .ns = ns)
  } else {
    out <- rlang::call2(fun, !!!args)
  }
  out
}
