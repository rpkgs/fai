#' IEX Connector
#'
#' @param token IEX-Token
#' @param sandbox Use sandbox mode
#' @param uri iexcloud URL
#'
#' @export
iex <- \(
  token,
  sandbox = FALSE,
  uri = sprintf("https://%s.iexapis.com/v1", ifelse(sandbox, "sandbox", "cloud"))
) {
  list(token = token, uri = uri)
}

set_new_connector(
  connector = "iex",
  func = list(func = "iex", pkg = "pfennigbaum")
)
